# Vagrant - LAMP

This is a Vagrant configuration for a local LAMP development server, running Ubuntu 14.04 it includes Apache, PHP and necessary libraries/extensions, mySQL, Compass, SASS, and MailHog.

## Installation

* Clone the Repository to a sensible place, ~/development/env/lamp works nicely
* Make the `addlocal` tool available globally, if prompted for a password use your computer user account password
    *   While in the base repo directory run the command `sudo cp addlocal /usr/local/bin/addlocal`
    *   Make it executable by running `sudo chmod +x /usr/local/bin/addlocal`
* Navigate to the `puphpet` directory and copy `config-custom.yaml.template` as `config-custom.yaml`
* Edit `config-custom.yaml` and set line 6 to match your projects directory
* Boot it up, type "vagrant up" and follow any prompts

## Using the addlocal tool
The `addlocal` commandline tool will add a new site within the Vagrant LAMP instance and update your local hosts file to point at it. To use it type `addlocal <sitename>`.

### Example usage

`addlocal mysite`

Outputs the following and creates the site config, if you're prompted for your password please enter it.
```
Running Vagrant provision process
==> lamp: Running provisioner: shell...
    lamp: Running: inline script
==> lamp: Running provisioner: shell...
...
...
...
==> lamp: Running provisioner: shell...
    lamp: Running: /var/folders/bm/yzzpgy8n2wq1wgx8kpb6zh_h0000gn/T/vagrant-shell20160309-6034-vjvx1k.sh

Adding line to /etc/hosts
Password:

Successfully ammended /etc/hosts, you're good to go. 

Configuration for site mysite added to /Users/benanderton/development/env/lamp/config-custom.yaml.
	URL: mysite.dev
	Local Directory: /Users/benanderton/development/projects/mysite/site
```


